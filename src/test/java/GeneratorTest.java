import junit.framework.TestCase;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GeneratorTest extends TestCase {

    @Test
    public void testWriteRandomStringsToFile() throws Exception {
        for (int fileSizeInMb = 1; fileSizeInMb < 10; fileSizeInMb *= 10) {
            Generator g = new Generator(Paths.get(String.valueOf(fileSizeInMb)));
            g.writeRandomStringsToFile(fileSizeInMb, 30, 100);
            assertTrue(Files.exists(Paths.get(String.valueOf(fileSizeInMb))));
            final long outputSize = Files.size(Paths.get(String.valueOf(fileSizeInMb)));
            assertEquals(fileSizeInMb * (double) 1048576, outputSize, 1000);
        }
    }

    private void checkSizes(Generator g, int fileSizeInMb) throws IOException {

    }

    @Test
    public void testPercentageComplete() throws Exception {
        Generator g = new Generator(Paths.get("test"));
        assertEquals(10.0, g.percentageComplete(100, 10));

    }

    @Test
    public void testRandInt() throws Exception {
        Generator g = new Generator(Paths.get("test"));
        assert (20 - 10 <= g.randInt(10, 20));
    }
}