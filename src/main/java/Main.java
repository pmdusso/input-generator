import java.nio.file.Paths;

/**
 * Created by pmdusso on 13.04.15.
 */
public class Main {


    public static void main(String[] args) throws Exception {

        if (args.length == 0) {
            System.out.println("Input   : <fileSizeInMb> <minRecordSize> <maxRecordSize>");
            System.out.println("Output  : <fileSizeInMb_minRecordSize_maxRecordSize>");
            System.out.println();

            System.out.println("Constraints:");
            System.out.println("minRecordSize > 0 && minRecordSize <= maxRecordSize");
            return;
        }

        int fileSizeInMb;
        int minRecordSize;
        int maxRecordSize;
        String output = String.format("%s_%s_%s", args[0], args[1], args[2]);

        fileSizeInMb = parseInput(args[0]);
        minRecordSize = parseInput(args[1]);
        maxRecordSize = parseInput(args[2]);
        checkConstraints(fileSizeInMb, minRecordSize, maxRecordSize);


        Generator g = new Generator(Paths.get(output));
        g.writeRandomStringsToFile(fileSizeInMb, minRecordSize, maxRecordSize);
        System.out.println("File generation finished.");
    }


    public static int parseInput(String args) {
        int input;
        try {
            input = Integer.valueOf(args);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
            throw ex;
        }
        return input;
    }

    public static void checkConstraints(int fileSizeInMb, int minRecordSize, int maxRecordSize) {
        if (fileSizeInMb < 0) {
            throw new IllegalArgumentException("fileSizeInMb must be greater than 0");
        }
        if (minRecordSize < 0) {
            throw new IllegalArgumentException("minRecordSize must be greater than 0");
        }
        if (minRecordSize > maxRecordSize) {
            throw new IllegalArgumentException("minRecordSize must be smaller or equal to maxRecordSize");
        }
    }


}
