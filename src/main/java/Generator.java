import org.apache.commons.lang3.RandomStringUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

/**
 * Created by pmdusso on 13.04.15.
 */
public class Generator {

    private static final Charset charset = Charset.forName("UTF-8");

    private Path file;
    private BufferedWriter bufferedWriter;

    private Random rand;
    private RandomStringUtils randomString;

    public Generator(Path output) throws IOException {

        if (Files.exists(output)) {
            Files.delete(output);
        }

        this.file = Files.createFile(output);
        bufferedWriter = Files.newBufferedWriter(output, charset);

        this.rand = new Random(31);
        this.randomString = new RandomStringUtils();
    }

    public void writeRandomStringsToFile(int fileSizeInMb, int minRecordSize, int maxRecordSize) {
        double fileMaxSize = (fileSizeInMb * (double) 1048576);
        double fileCurrentSize = 0;

        try {

            while (fileCurrentSize <= fileMaxSize) {
                String line = getRandomString(randomString, minRecordSize, maxRecordSize) + "\n";

                byte[] inBytes = getStringInBytes(line);
                fileCurrentSize += inBytes.length;

                if (fileCurrentSize <= fileMaxSize)
                    bufferedWriter.write(line, 0, line.length());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedWriter.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public double percentageComplete(double fileMaxSize, double fileCurrentSize) {
        final double percentage = (fileCurrentSize * 100) / fileMaxSize;
        System.out.println(percentage +  "% completed...");
        return percentage;
    }

    private String getRandomString(RandomStringUtils randomString, int minRecordSize, int maxRecordSize) {
        return randomString.random(randInt(minRecordSize, maxRecordSize), 0, 0, true, true, null, rand);
    }

    private byte[] getStringInBytes(String string) {
        return string.getBytes(Charset.forName("UTF-8"));
    }

    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     *
     * @see java.util.Random#nextInt(int)
     */
    public int randInt(int min, int max) {
        if (rand == null)
            rand = new Random(31);

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }
}
