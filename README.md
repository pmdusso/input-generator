### Input Generator

* Input   : <fileSizeInMb> <minRecordSize> <maxRecordSize>"
* Output  : <fileSizeInMb_minRecordSize_maxRecordSize>

* Constraints:
* minRecordSize > 0 && minRecordSize <= maxRecordSize"